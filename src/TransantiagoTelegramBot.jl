module TransantiagoTelegramBot

import Dates
import HTTP

struct Sesión
    galletas::Dict{String, Set{HTTP.Cookie}}
    momento_obtenido::Dates.DateTime
    duración_galleta::Dates.Hour
end
Sesión(galletas, momento_obtenido) = Sesión(galletas, momento_obtenido, Dates.Hour(2)) 

"""Entrega una Sesión nueva
"""
function nueva_sesión()
    galletas = Dict{String, Set{HTTP.Cookie}}()
    r = HTTP.get("http://web.smsbus.cl/web/buscarAction.do?d=cargarServicios",
                 cookies=true,
                 cookiejar=galletas)
    Sesión(galletas, Dates.now())
end


end # module
